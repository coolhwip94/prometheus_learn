# Prometheus_Client
> This repo holds notes for prometheus client.  
> A way to output metrics for consumption in prometheus and/or use in grafana.

## Overview
---
> Prometheus client provides four different types of metric

- `Counter` : Counters go up, and reset when the process restarts.
- `Gauge` : Gauges can go up and down.
- `Summary` : Summaries track the size and number of events.
- `Histogram` : Histograms track the size and number of events in buckets. This allows for aggregatable calculation of quantiles.


## Running
---
> `main.py` will demonstrate a fastapi that outputs metrics using prometheus client  
> these metrics can then be consumed by prometheus and displayed in grafana

- Setup python environment
  ```
  # create virtual environment
  python -m venv venv

  # activate virtual environment
  source venv/bin/activate

  # install requirements
  pip install -r requirements.txt
  ```
- Running application
  ```
  python main.py
  # or
  uvicorn main:app --reload
  ```
- Access
  ```
  http://localhost:8000/metrics
  ```

## References
---
- https://github.com/prometheus/client_python/blob/master/README.md

