from prometheus_client import Counter, generate_latest, Gauge, Summary, Histogram, Info
import random
from fastapi import FastAPI, Response
import uvicorn
import psutil


CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')
VERSION = "1.0.0"

# example counter
count = Counter('num_requests', 'The number of requests.')

# example guages
memory_guage = Gauge(
    'memory_in_gb', 'The amount of memory remaining on this server in GB.')
ram_guage = Gauge('ram_in_gb', 'The amount of ram being used on this server')

# example summary
latency_summary = Summary('memory_calc_latency_seconds',
                          'Summary of memory calc latency')

# example historgram
histogram_example = Histogram(
    'histogram_calc_ram_used', 'Histogram of ram used')

# example info
i = Info('my_build_version', 'Description of info')
i.info({'version': VERSION})

# fastapi default instance
app = FastAPI(version=VERSION)


@latency_summary.time()  # use latency summary here for example
def calc_memory_remaining():
    disk_free = psutil.disk_usage('/').free / (1024.0 ** 3)  # calculate gigs
    memory_remaining = round(disk_free, 2)  # function to get remaining memory
    return memory_remaining


def calc_ram_used():
    ram_used = psutil.virtual_memory().used / (1024.0 ** 3)
    ram_used = round(ram_used, 2)
    return ram_used


@app.get("/metrics")
async def get_data():

    """Returns all data as plaintext."""

    count.inc()  # incremenet count by 1 to show endpoint being called

    # Sets the gauge to an exact value
    memory_guage.set(calc_memory_remaining())
    ram_guage.set(calc_ram_used())  # set ram guage to used

    # histogram of randim int data
    histogram_example.observe(random.randint(0, 9))

    # use prometheus's built in `generate_latest()` to gather recent metrics
    # format to plaint txt
    return Response(generate_latest(), media_type=CONTENT_TYPE_LATEST)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
